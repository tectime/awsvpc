variable "stage_or_prod" {}

variable "vpc_cidr_block" {
  description = "CIDR block for main/application VPC."
}

# Provision frontend in multiple subnets?
variable "frontend_multi_az" {
  default = false
}

# Provision backend in multiple subnets?
variable "backend_multi_az" {
  default = false
}

variable "resource_tags" {
  type = "map"
}
