locals {
  module_tags = {
    "Module ID"      = "fe3ba717-0e05-4e44-8948-b8ca55a65dba"
    "Module Version" = "0.1.1"
    "Module Name"    = "VPC"
  }

  local_tags = "${merge(var.resource_tags,local.module_tags)}"
}

// Idea: Create VPC in Stage or Prod Env only.
locals {
  is_stage = "${var.stage_or_prod == "Stage" ? 1 : 0}"
  is_prod  = "${var.stage_or_prod == "Prod" ? 1 : 0}"
}

// Be aware of potential unintended destroy actions if not stage and not prod.
resource "aws_vpc" "vpc" {
  count                = "${local.is_stage || local.is_prod ? 1 : 0}"
  cidr_block           = "${var.vpc_cidr_block}"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags                 = "${local.local_tags}"
}

module "config" {
  source          = "../../modules/config"
  stage_or_prod   = ""
  caller_basename = "${title(basename(path.module))}"
}

/* The AZ data source allows access to the list of AWS AZs 
which can be accessed by an AWS account within the region configured in the provider. */
data "aws_availability_zones" "available" {}

//Create frontend subnet(s) in each AZ of the current region.
resource "aws_subnet" "frontend" {
  count                   = "${var.frontend_multi_az ? length(data.aws_availability_zones.available.names) : 1}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  availability_zone       = "${element(sort(data.aws_availability_zones.available.names),count.index)}"
  cidr_block              = "${cidrsubnet(aws_vpc.vpc.cidr_block, 8, "${count.index+1}")}"
  map_public_ip_on_launch = "true"
  tags                    = "${merge(local.local_tags,map("Nettype","${module.config.config_frontend_subnet_tag}"))}"
}

//Create backend subnet(s) in one AZ of the current region.
resource "aws_subnet" "backend" {
  count                   = "${var.backend_multi_az ? length(data.aws_availability_zones.available.names) : 1}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  availability_zone       = "${element(sort(data.aws_availability_zones.available.names),count.index)}"
  cidr_block              = "${cidrsubnet(aws_vpc.vpc.cidr_block, 8, "${count.index+11}")}"
  map_public_ip_on_launch = "false"
  tags                    = "${merge(local.local_tags,map("Nettype","${module.config.config_backend_subnet_tag}"))}"
}

// Return VPC ID.
output "aws_vpc_id" {
  value = "${aws_vpc.vpc.id}"
}
