terraform {
  required_version = "~> 0.11"
}

output "config_global_tags" {
  value = {
    "Application Name" = "TJKG Test"
    "Application ID"   = "892db31d-85ac-4504-b708-9c87215137d6"
    "Application Role" = ""
    "Cluster"          = "0"
    "Caller"           = "${var.stage_or_prod}_${var.caller_basename}"
    "Security"         = "Unencrypted"
    "Environment"      = "${var.stage_or_prod}"
    "Owner"            = "Klier - Goedicke"
    "Customer"         = "Bayer"
    "Project"          = ""
    "Confidentiality"  = "Public"
    "Compliance"       = "Base Compliance"
  }
}

output "config_frontend_subnet_tag" {
  value = "Frontend"
}

output "config_backend_subnet_tag" {
  value = "Backend"
}
