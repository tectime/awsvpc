locals {
  module_tags = {
    "Module ID"      = "2e4d1c36-2305-4f75-a2b0-1cdcec0857be"
    "Module Version" = "0.1.1"
    "Module Name"    = "SG"
  }

  local_tags = "${merge(var.resource_tags,local.module_tags)}"
}

// Web rules.
resource "aws_security_group" "web" {
  count = "${contains(var.service_types, "web") ? 1 : 0}"
  name = "sg-${var.stage_or_prod}-web"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "allow_http_inbound" {
  count = "${contains(var.service_types, "web") ? 1 : 0}"
  type = "ingress"
  security_group_id = "${aws_security_group.web.id}"
  from_port = 8080
  to_port = 8080
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_http_outbound" {
  count = "${contains(var.service_types, "web") ? 1 : 0}"
  type = "egress"
  security_group_id = "${aws_security_group.web.id}"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

// DB rules.
resource "aws_security_group" "db" {
  count = "${contains(var.service_types, "db") ? 1 : 0}"
  name = "sg-${var.stage_or_prod}-db"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "allow_tsql_inbound" {
  count = "${contains(var.service_types, "db") ? 1 : 0}"
  type = "ingress"
  security_group_id = "${aws_security_group.db.id}"
  from_port = 1433
  to_port = 1433
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

// Return sg instance id(s).
output "aws_sg_ids" {
  value = ["${aws_security_group.web.*.id}"]
}