variable "stage_or_prod" {}

// for web or db server?
variable "service_types" {
    type = "list"
}

variable "vpc_id" {}

variable "resource_tags" {
  type = "map"
}