locals {
  module_tags = {
    "Module ID"      = "44185010-bc76-4ac1-97c0-6efe82ab42cc"
    "Module Version" = "0.1.1"
    "Module Name"    = "EC2"
  }

  local_tags = "${merge(var.resource_tags,local.module_tags)}"
}

resource "aws_key_pair" "awssshkey" {
  key_name   = "${var.aws_sshkey_name}"
  public_key = "${file(var.aws_sshkey_path)}"
}

// Create EC2 instance(s) in subnet(s).
resource "aws_instance" "frontend_web" {
  count         = "${var.service_type == "web" ? "${length(var.subnet_ids)}" : 0}"
  ami           = "ami-df8406b0"
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.awssshkey.key_name}"
  tags          = "${local.local_tags}"
  subnet_id     = "${var.subnet_ids[count.index]}"
  vpc_security_group_ids = ["${var.vpc_security_group_ids}"]
}

// Return instance id(s).
output "aws_instance_id" {
  value = "${aws_instance.frontend_web.*.id}"
}
