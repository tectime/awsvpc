variable "aws_sshkey_path" {}
variable "aws_sshkey_name" {}
variable "stage_or_prod" {}

// frontend or backend?
// variable "net_type" {}
variable "subnet_ids" {
  type = "list"
}

// web or db server?
variable "service_type" {}

variable "resource_tags" {
  type = "map"
}

variable "vpc_security_group_ids" {
  type = "list"
}

