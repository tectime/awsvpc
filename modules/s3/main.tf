//https://blog.james-carr.org/using-templates-in-terraform-17bb8f4a0aac

locals {
  module_tags = {
    "Module ID"      = "87445a14-fbff-485b-9468-f4ecbf2d5b8f"
    "Module Version" = "0.1.1"
    "Module Name"    = "S3"
  }

  local_tags = "${merge(var.resource_tags,local.module_tags)}"
}