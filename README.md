Build Generic AWS 2-Tier Infrastructure with Terraform

stage/vpc
 - Create VPC with multi-AZ support in a specified AWS region.
 - Create frontend subnets and backend subnets.

stage/web
- Create EC2 instances in frontend subnets.
- Create appropriate SGs for frontend subnets.

stage/db
- Create DB instance(s) in backend subnets.
- Create appropriate SGs in backend subnets.

To make the code work you must add following files

- stage/[vpc/web/db]/terraform.tfvars
\naws_key = "your-aws-key"
\naws_secret = "your-aws-secret"
- resources/stage/private/webserv.pub
- resources/stage/private/dbserv.pub