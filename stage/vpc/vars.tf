// Secret injections from tfvars.
// Env specific settings.
variable "aws_region" {
  description = "AWS region."
  default     = "eu-west-1"
}

// Resource type specific.
variable "aws_vpc_cidr" {
  description = "VPC CIDR block"
  default     = "10.1.0.0/16"
}
