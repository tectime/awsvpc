// Generic head.
provider "aws" {
}

locals {
  stage_or_prod = "${title(basename(dirname(path.module)))}"
}

module "config" {
  source          = "../../modules/config"
  stage_or_prod   = "${local.stage_or_prod}"
  caller_basename = "${title(basename(path.module))}"
}

// Create a stage/prod VPC.
module "vpc" {
  source            = "../../modules/vpc"
  vpc_cidr_block    = "${var.aws_vpc_cidr}"
  frontend_multi_az = "true"
  backend_multi_az  = "false"
  resource_tags     = "${module.config.config_global_tags}"
  stage_or_prod     = "${local.stage_or_prod}"
}

output "stage_or_prod" {
  value = "${local.stage_or_prod}"
}

output "aws_vpc_id" {
  value = "${module.vpc.aws_vpc_id}"
}
