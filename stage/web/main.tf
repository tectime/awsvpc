// Generic head.
provider "aws" {
  access_key = "${var.aws_key}"
  secret_key = "${var.aws_secret}"
  region     = "${var.aws_region}"
}

locals {
  stage_or_prod = "${title(basename(dirname(path.module)))}"
}

module "config" {
  source          = "../../modules/config"
  stage_or_prod   = "${local.stage_or_prod}"
  caller_basename = "${title(basename(path.module))}"
}

// Create security group and ingress & egress rules.
module "sg" {
  source                 = "../../modules/sg"
  stage_or_prod = "${local.stage_or_prod}"
  service_types  = ["remote","web"]
  vpc_id        = "${var.vpc_id}"
  resource_tags = "${module.config.config_global_tags}"
}

// Find subnets tagged with frontend and create a web instance in each subnet.
data "aws_subnet_ids" "subnet" {
  vpc_id = "${var.vpc_id}"

  tags = {
    Nettype = "${module.config.config_frontend_subnet_tag}"
  }
}

// Create web instance(s) - in the right stage/prod VPC.
module "ec2" {
  source                 = "../../modules/ec2"
  aws_sshkey_path        = "${path.module}/../../resources/${var.stage_or_prod}/private/websrv.pub"
  aws_sshkey_name        = "websrvkey"
  stage_or_prod          = "${local.stage_or_prod}"
  resource_tags          = "${module.config.config_global_tags}"
  service_type           = "web"
  vpc_security_group_ids = ["${module.sg.aws_sg_ids}"]
  // Unfortunately count variables are only valid within resources.
  // Hence, we must count inside the module, actually.
  // count        = "${length(data.aws_subnet_ids.frontend.ids)}" // <--- Doesn't work.
  subnet_ids = "${data.aws_subnet_ids.subnet.ids}" // <--- Count over subnet ids inside module instead.  
}
