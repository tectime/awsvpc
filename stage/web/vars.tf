// Secret injections from tfvars.
variable "aws_key" {
  description = "AWS access key."
}

variable "aws_secret" {
  description = "AWS secret key."
}

// Env specific settings.
variable "aws_region" {
  description = "AWS region."
  default     = "eu-central-1"
}

variable "stage_or_prod" {
  description = "Is this Terraform code for stage or for prod environment?"
  default     = "Stage"
}

// Resource type specific.
variable "vpc_id" {
  description = "ID of the VPC that is supposed for THIS stage/prod environment."
  default     = "vpc-4a48b421"
}
